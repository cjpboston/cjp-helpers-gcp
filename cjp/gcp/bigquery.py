from urllib3.exceptions import ProtocolError
from google.cloud.bigquery import retry


# default bq predicate + 104 Error
def _retry_if_bqdefault_or_104(exception: Exception):
    """Exception predicate for the google.bigquery.retry function.
    Retries if it's one of the default transient errors (google.bigquery.retry._should_retry) 
    or if it's the urllib3.exceptions.ProtocolError() with 104 as the cause. 

    Args:
        exception (Exception): exception raised by retryable function.
    """
    print("_retry_if_bqdefault_or_104 called with this error:")
    print(exception)
    if retry._should_retry(exception):
        return True
    if isinstance(exception, ProtocolError):
        print('caught urllib3.exceptions.ProtocolError')
        # parsing out the 104:
        # urllib3.exceptions.ProtocolError: ('Connection aborted.', ConnectionResetError(104, 'Connection reset by peer'))
        if exception.args[1].args[0] == 104:
            print('retrying urllib3.exceptions.ProtocolError; 104 check passed.')
            return True
    return False
