from google.cloud import datastore

# TODO: create test cases, change from module to library, upload to git for reuse.


def generate_entities_from_df(df, project, entity_kind: str, key_column: str = None):
    """Given a dataframe, returns a list of gcp DataStore Entities. Modifies dataframe in place."""
    # if we pass a key column...
    if key_column:
        # setup a new column called ds_entity
        # populated with datastore.key objects described by x which is the value in the key_column parameter
        df["ds_entity"] = df[key_column].apply(
            lambda x: datastore.Key(entity_kind, x, project=project)
        )
    # no key column specified
    # populated with datastore.key objects as unique values provided by GCP datastore
    else:

        df["ds_entity"] = df.apply(
            lambda x: datastore.Key(entity_kind, project=project), axis=1
        )

    # modifies ds_entity from a datastore key to a datastore entity
    df["ds_entity"] = df.ds_entity.apply(
        lambda x: datastore.Entity(key=x)
    )  # creates entities

    # modify the entity column values to contain the data columns
    df.apply(lambda x: _populate_entity_with_df_row(x, "ds_entity"), axis=1)

    # create a list from the entity in each row
    entities = df["ds_entity"].tolist()

    # get rid of this infernal ds_entity column!
    df.drop(columns=["ds_entity"], inplace=True)

    return entities


def _populate_entity_with_df_row(df_row, entity_col: str):
    """Helper function for working with pandas dataframes of gcp datastore entities"""
    # turn df_row into a dictionary version of what was in the row
    df_row = df_row.to_dict()

    # setup entity as a reference to the Entity object in the entity_col value of df_row
    # df_row no longer has the key 'entity_col' but it has all the other data that was in the row
    entity = df_row.pop(entity_col)

    # update the entity object so it now has the data columns left in df_row
    entity.update(df_row)


def _delete_kind_from_ds(ds_client, kind: str):
    """Given a Cloud Datastore client and an entity kind,
    deletes all records of that entity kind. Use for testing only."""
    entity_keys = get_all_keys_of_kind(ds_client, kind)
    paging_delete(ds_client, entity_keys)


def get_all_keys_of_kind(ds_client, kind: str):
    """Returns all a list of keys of kind."""
    entity_query = ds_client.query(kind=kind)
    entity_query.keys_only()
    entities = list(entity_query.fetch())
    return [e.key for e in entities]


def refresh_entities(ds_client, entities, kind: str, prefer_download=True):
    if prefer_download:
        _refresh_entities_download(ds_client, entities, kind)
    else:
        _refresh_entities_upload(ds_client, entities, kind)


def _refresh_entities_download(ds_client, entities, kind: str):
    """Given a list of entities of the same kind:
        - downloads all entities of that kind
        - compares downloaded entities to passed entities
        - for entities that changed
            - uploads replacements for deleted entities
        - uploads new entities"""
    # creating a set of partial keys crashes the app.
    if sum([e.key.is_partial for e in entities]):
        raise ValueError("Can't refresh entities with partial keys.")
    if len({e.key.kind for e in entities}) > 1:
        raise ValueError("Can't refresh entities of mixed kinds.")
    if entities[0].key.kind != kind:
        raise ValueError("Kind of entities and kind passed don't match.")
    # create empty lists
    to_upload = []
    to_delete = []
    # setup a dictionary of the entities passed in
    entities = {e.key: e for e in entities}
    # download all existing entity keys
    old_entities_qry = ds_client.query(kind=kind)
    old_entities_qry.keys_only()
    old_entity_keys = {e.key for e in old_entities_qry.fetch()}
    # check if the old key is in the updated entities, if not extend to_delete
    to_delete.extend(list(old_entity_keys - (old_entity_keys & entities.keys())))
    # download potentially updated entities.
    # todo: change from downloading all entities at once to downloading and processing in 1000s.
    old_entity_keys = list(old_entity_keys & entities.keys())
    old_entities = paging_get(ds_client, old_entity_keys)
    for oe in old_entities:
        ne = entities.pop(oe.key)
        # if they don't match then we must update the datastore
        if oe != ne:
            to_upload.append(ne)
    # add new entites to upload list.
    to_upload.extend(entities.values())
    paging_delete(ds_client, to_delete)
    paging_put(ds_client, to_upload)


def _refresh_entities_upload(ds_client, entities, kind: str):
    """Given a list of entities of the same kind, upserts entities and deletes any that weren't upserted."""
    # creating a set of partial keys crashes the app.
    if sum([e.key.is_partial for e in entities]):
        raise ValueError("Can't refresh entities with partial keys.")
    print("refresh entities getting old keys")  # for debugging only
    # get a set of the pre-existing entity keys on the datastore
    old_keys = set(get_all_keys_of_kind(ds_client, kind))
    print("refresh entities making new keys")  # for debugging only
    # this line is freezing for some reason
    new_keys = {e.key for e in entities}
    print("refresh entities keys to delete")  # for debugging only
    # instead of deleting everything before re-loading,
    # this calculates the keys that won't be overwritten
    # (so they can be deleted in a smaller job)
    keys_to_delete = list(old_keys - new_keys)
    print("refresh entities paging put")  # for debugging only
    # save entities to datastore, overwriting existing entities
    paging_put(ds_client, entities)
    print("refresh entities paging delete")  # for debugging only
    # this deletes any keys that won't be overwritten by the paging_put
    paging_delete(ds_client, keys_to_delete)


def paging_delete(ds_client, keys, step_size=500):
    """Calls client.delete_multi() in in chunks of step_size."""
    for left in range(0, len(keys), step_size):  # Batch deletes from datastore.
        ds_client.delete_multi(keys[left : left + step_size])


def paging_put(ds_client, entities, step_size=500):
    """Calls client.put_multi() in in chunks of step_size."""
    for left in range(0, len(entities), step_size):  # Batch uploads to gcp.
        ds_client.put_multi(entities[left : left + step_size])


def paging_get(ds_client, keys, step_size=1000):
    """Calls client.get_multi() in in chunks of step_size."""
    results = []
    for left in range(0, len(keys), step_size):  # Batch deletes from datastore.
        results.extend(ds_client.get_multi(keys[left : left + step_size]))
    return results
