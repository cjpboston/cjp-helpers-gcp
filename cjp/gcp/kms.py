from google.cloud import kms
from google.cloud import storage


def load_credentials(
    keyring,
    key,
    kms_project="cjp-secrets-kms",
    keyring_location="global",
    filename="secret.txt.encrypted",
    bucket=None,
    storage_client=None,
    kms_client=None,
) -> (str, str):
    """Downloads encrypted file w/username password, decrypts via kms,
    returns tuple (username, password)
    Assumes password file follows format: username\\npassword\\n
    """
    if not bucket:
        bucket = key
    print(f"Loading encrypted credentials file: gs://{bucket}/{filename}")
    if not storage_client:
        storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket)
    secret = bucket.get_blob(filename)
    if not secret:
        raise ValueError(f"Bucket {bucket} doesn't contain {filename}")
    print(
        f"Decrypting credentials with key: {keyring_location} {keyring}/{key} on project {kms_project}"
    )
    if not kms_client:
        kms_client = kms.KeyManagementServiceClient()
    key_name = kms_client.crypto_key_path(kms_project, keyring_location, keyring, key)
    # the old decrypt statement, works with kms version <= 1.2.1
    # secret = kms_client.decrypt(key_name, secret.download_as_string())
    # testing the new decrypyt statement, works with kms > 1.2.1 including current 2.12.1
    secret = kms_client.decrypt(
        request={"name": key_name, "ciphertext": secret.download_as_string()}
    )

    # print(secret)
    secret = secret.plaintext.decode()
    u, p = secret.strip().split()
    return u, p


# storage_client = storage.Client()
# kms_client = kms.KeyManagementServiceClient()

# secret = load_credentials(
#     "super-dooper-secret-keyring",
#     "super-dooper-secret-key",
#     kms_project="cjp-kms-test",
#     storage_client=storage_client,
#     kms_client=kms_client,
# )
# secret
