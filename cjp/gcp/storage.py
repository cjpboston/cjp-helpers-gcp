from google.cloud import storage
from google.api_core import exceptions as gcp_exceptions
from time import sleep


def upload_file_to_storage(
    file_buffer,
    blob_path: str,
    bucket: str,
    log_prefix: str,
    gs_client: storage.Client,
    gs_retries: int = 3,
    retry_backoff: int = 1,
):
    """Uploads file in file_buffer to as blob_path to the bucket.
    Retries w/1s backoff in case of 410 gs error. (transient error.)
    
    Arguments:
        file_buffer {file_buffer} -- File to upload, open for reading.
        blob_path {str} -- filename/path for storage blob. destination to gs://bucket/blob_path
        bucket {str} -- name of target google storage bucket.
        log_prefix {str} -- prefix for printed statements (consumed by gcp logging)
        gs_client {storage.Client} -- google.cloud.storage Client object. (Authenticated.)

    Keyword Arguments:
        gs_retries {int} -- [description] (default: {3})
        retry_backoff {int} -- [description] (default: {1})
    """
    print(f"Uploading file gs://{bucket}/{blob_path}")
    bucket = gs_client.bucket(bucket)
    while gs_retries > 0:
        try:
            blob = bucket.blob(blob_path)
            blob.upload_from_file(file_buffer, rewind=True)
            break
        except gcp_exceptions.GoogleAPICallError as err:
            print(f"{log_prefix}: ERROR: caught API Call Error:\n {err}")
            gs_retries -= 1
            if (err.code != 410) or (gs_retries == 0):
                raise
            else:
                sleep(retry_backoff)
