#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_namespace_packages

# with open('README.rst') as readme_file:
#     readme = readme_file.read()

requirements = [
    "google-cloud-datastore",
    "google-cloud-kms",
    "google-cloud-storage",
    "google-cloud-bigquery",
]

setup(
    author="cjp",
    author_email="danylok@cjp.org",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
    ],
    description="A project namespace to contain CJP's of the occasionally useful helper functions. cjp.gcp for functions useful for working with google cloud.",
    install_requires=requirements,
    license="BSD license",
    include_package_data=True,
    name="cjp_helpers_gcp",
    packages=find_namespace_packages(),
    url="https://gitlab.com/cjpboston/cjp-helpers-gcp",
    version="0.1.7",  # <- update from 1.6 to reflect the edit to repair compatibility with current kms 2.x versions
    zip_safe=False,
)
